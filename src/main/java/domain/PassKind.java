package domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

/**
 * Miałem wykorzystać początkowo enumy, jednak trzymanie karnetów w bazie daje więcej możliwości, zwłaszcza zarządzania nimi
 */
@NamedQueries({
        @NamedQuery
                (
                        name = "getPassKindByName",
                        query = "FROM PassKind pk WHERE pk.name= :name"
                )
})
@Entity
public class PassKind extends AbstractEntity
{
    @NotNull
    private String name;
    @NotNull
    private float price;

    private int numberOfEntrances;
    private int validityDays;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public int getNumberOfEntrances()
    {
        return numberOfEntrances;
    }

    public void setNumberOfEntrances(int numberOfEntrances)
    {
        this.numberOfEntrances = numberOfEntrances;
    }

    public int getValidityDays()
    {
        return validityDays;
    }

    public void setValidityDays(int validityDays)
    {
        this.validityDays = validityDays;
    }
}
