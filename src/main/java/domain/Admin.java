package domain;


import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

/**
 * Informacje o kontach, które mają dostęp do systemu
 */
@NamedQueries({
        @NamedQuery
                (
                        name = "getAdminByLogin",
                        query = "FROM Admin a WHERE a.login= :login"
                )
})
@Entity
public class Admin extends AbstractEntity
{
    @NotNull
    private String name;
    @NotNull
    private String surname;
    @NotNull
    private String login;
    @NotNull
    private String password;

    public void setDefault()
    {
        name = "admin";
        surname = "admin";
        login = "admin";
        password = "admin";
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
