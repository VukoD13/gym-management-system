package domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 *
 */
@Entity
public class TransactionHistory extends AbstractEntity
{
    @ManyToOne
    @NotNull
    private Person person;

    @NotNull
    private float price;

    @OneToOne
    @NotNull
    private PassKind passKind;

    @NotNull
    private Date date;

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public float getPrice()
    {
        return price;
    }

    public void setPrice(float price)
    {
        this.price = price;
    }

    public PassKind getPassKind()
    {
        return passKind;
    }

    public void setPassKind(PassKind passKind)
    {
        this.passKind = passKind;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }
}
