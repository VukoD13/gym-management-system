package domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * Informacje o identyfikatorach (kluczykach)
 */
@Entity
@NamedQueries({
        @NamedQuery
                (
                        name = "getIdentifierByNumber",
                        query = "FROM Identifier i WHERE i.number= :number"
                )
})
public class Identifier extends AbstractEntity
{
    @NotNull
    private int number;

    @OneToOne
    private Person person;  //if null, kluczyk dostępny, else dana osoba jest w środku z danym kluczem

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }
}
