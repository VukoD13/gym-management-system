package domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Informacje o karnetach
 */
@Entity
public class Pass extends AbstractEntity
{
    @NotNull
    @OneToOne
    //@Enumerated(EnumType.STRING), int (ustawiony domyślnie) lepszy - mniejsze zużycie pamięci, karnetów w bazie może być sporo (tyle ile użytkowników)
    private PassKind passKind;

    /**
     * jesli jest null oznacza to, że w danym rodzaju karnetu jest to parametr nieważny
     */
    private Date expireDate;
    /**
     * jesli jest null oznacza to, że w danym rodzaju karnetu jest to parametr nieważny
     */
    private Integer numberOfEntrances; // patrz expire date to_do

    public Date getExpireDate()
    {
        return expireDate;
    }

    public void setExpireDate(Date expireDate)
    {
        this.expireDate = expireDate;
    }

    public PassKind getPassKind()
    {
        return passKind;
    }

    public void setPassKind(PassKind passKind)
    {
        this.passKind = passKind;
    }

    public Integer getNumberOfEntrances()
    {
        return numberOfEntrances;
    }

    public void setNumberOfEntrances(Integer numberOfEntrances)
    {
        this.numberOfEntrances = numberOfEntrances;
    }
}
