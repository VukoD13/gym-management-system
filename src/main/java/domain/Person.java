package domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

/**
 * Informacje o klientach
 */
@Entity
@NamedQueries({
    @NamedQuery
    (
        name = "getPersonBySurnameAndName",
        query = "FROM Person p WHERE p.surname= :surname AND p.name= :name"
    )
})
public class Person extends AbstractEntity
{
    @NotNull
    private String name;
    @NotNull
    private String surname;

    @OneToOne //default: (fetch = FetchType.LAZY)
    private Pass pass;

    private Boolean isIn = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Pass getPass() {
        return pass;
    }

    public void setPass(Pass pass) {
        this.pass = pass;
    }

    public Boolean getIsIn()
    {
        return isIn;
    }

    public void setIsIn(Boolean isIn)
    {
        this.isIn = isIn;
    }
}
