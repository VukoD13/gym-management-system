package repositories;

/**
 *
 */
public interface IRepositoryCatalog
{
    public IPersonRepository getPersons();
    public IAdminRepository getAdmins();
    public IPassRepository getPasses();
    public IPassKindRepository getPassKinds();
    public IIdentifierRepository getIdentifiers();
    public ITransactionHistoryRepository getTransactionHistories();
}
