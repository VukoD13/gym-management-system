package repositories;

/**
 *
 */

import domain.AbstractEntity;

import java.util.List;

public interface IRepository <TEntity extends AbstractEntity>
{
    public void add(TEntity entity);
    public void update(TEntity entity);
    public void delete(TEntity entity);
    public List <TEntity> getAll(Class c);
}