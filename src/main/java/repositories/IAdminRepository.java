package repositories;

import domain.Admin;

/**
 *
 */
public interface IAdminRepository extends IRepository<Admin>
{
    public Admin getAdminByLogin(String login);
}
