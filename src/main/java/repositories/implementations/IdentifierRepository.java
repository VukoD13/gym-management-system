package repositories.implementations;

import beans.util.HibernateUtil;
import domain.Identifier;
import org.hibernate.Query;
import org.hibernate.Session;
import repositories.IIdentifierRepository;

import java.util.List;

/**
 *
 */
public class IdentifierRepository extends EntityRepository<Identifier> implements IIdentifierRepository
{
    @Override
    public Identifier getIdentifierByNumber(int number)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            Query query = session.getNamedQuery("getIdentifierByNumber")
                    .setParameter("number", number);

            List results = query.list();

            if (results.isEmpty())
            {
                return null;
            }

            return (Identifier) results.get(0); //zakładam, że istnieje w bazie tylko jedna osoba o takim imieniu i nazwisko
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }

        return null;
    }
}
