package repositories.implementations;

import repositories.*;

import javax.ejb.Stateless;

/**
 *
 */
@Stateless
public class RepositoryCatalog implements IRepositoryCatalog
{
    public RepositoryCatalog()
    {}

    @Override
    public IPersonRepository getPersons()
    {
        return new PersonRepository();
    }

    @Override
    public IAdminRepository getAdmins()
    {
        return new AdminRepository();
    }

    @Override
    public IPassRepository getPasses()
    {
        return new PassRepository();
    }

    @Override
    public IPassKindRepository getPassKinds()
    {
        return new PassKindRepository();
    }

    @Override
    public IIdentifierRepository getIdentifiers()
    {
        return new IdentifierRepository();
    }

    @Override
    public ITransactionHistoryRepository getTransactionHistories()
    {
        return new TransactionHistoryRepository();
    }

}
