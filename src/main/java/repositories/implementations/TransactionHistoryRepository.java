package repositories.implementations;

import domain.TransactionHistory;
import repositories.ITransactionHistoryRepository;

/**
 *
 */
public class TransactionHistoryRepository extends EntityRepository<TransactionHistory> implements ITransactionHistoryRepository
{}
