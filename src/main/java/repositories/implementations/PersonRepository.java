package repositories.implementations;

import beans.util.HibernateUtil;
import domain.Person;
import org.hibernate.Query;
import org.hibernate.Session;
import repositories.IPersonRepository;

import java.util.List;

/**
 *
 */
public class PersonRepository extends EntityRepository<Person> implements IPersonRepository
{
    @Override
    public Person getPersonBySurnameAndName(String surname, String name)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            Query query = session.getNamedQuery("getPersonBySurnameAndName")
                    .setParameter("surname", surname)
                    .setParameter("name", name);

            List results = query.list();

            if (results.isEmpty())
            {
                return null;
            }

            return (Person) results.get(0); //zakładam, że istnieje w bazie tylko jedna osoba o takim imieniu i nazwisko
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }

        return null;
    }
}
