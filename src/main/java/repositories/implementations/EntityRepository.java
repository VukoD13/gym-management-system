package repositories.implementations;

import beans.util.HibernateUtil;
import domain.AbstractEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repositories.IRepository;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class EntityRepository<TEntity extends AbstractEntity> implements IRepository<TEntity>
{
    @Override
    public void add(TEntity entity)
    {
        Transaction transaction = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            transaction = session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
        catch (RuntimeException e)
        {
            if (transaction != null)
            {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        finally
        {
            //session.flush();
            session.close();
        }
    }

    @Override
    public void update(TEntity entity)
    {
        Transaction transaction = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            transaction = session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
        catch (RuntimeException e)
        {
            if (transaction != null)
            {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }
    }

    @Override
    public void delete(TEntity entity)
    {
        Transaction transaction = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            transaction = session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        }
        catch (RuntimeException e)
        {
            if (transaction != null)
            {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }
    }

    @Override
    public List<TEntity> getAll(Class c)
    {
        String className = c.getSimpleName();
        List<TEntity> list = new ArrayList<TEntity>();

        Transaction transaction = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            transaction = session.beginTransaction();

            list = session.createQuery("from " + className).list();

            session.getTransaction().commit();

        }
        catch (RuntimeException e)
        {
            if (transaction != null)
            {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }

        return list;
    }
}
