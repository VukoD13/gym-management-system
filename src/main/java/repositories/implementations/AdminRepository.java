package repositories.implementations;

import beans.util.HibernateUtil;
import domain.Admin;
import org.hibernate.Query;
import org.hibernate.Session;
import repositories.IAdminRepository;

import java.util.List;

/**
 *
 */
public class AdminRepository extends EntityRepository<Admin> implements IAdminRepository
{
    @Override
    public Admin getAdminByLogin(String login)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            Query query = session.getNamedQuery("getAdminByLogin")
                    .setParameter("login", login);

            List results = query.list();

            if (results.isEmpty())
            {
                return null;
            }

            return (Admin) results.get(0); //zakładam, że istnieje w bazie tylko jeden admin o danym loginie
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }

        return null;
    }
}