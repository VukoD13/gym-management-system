package repositories.implementations;

import beans.util.HibernateUtil;
import domain.PassKind;
import org.hibernate.Query;
import org.hibernate.Session;
import repositories.IPassKindRepository;

import java.util.List;

/**
 *
 */
public class PassKindRepository extends EntityRepository<PassKind> implements IPassKindRepository
{
    public PassKind getPassKindByName(String name)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        try
        {
            Query query = session.getNamedQuery("getPassKindByName")
                    .setParameter("name", name);

            List results = query.list();

            if (results.isEmpty())
            {
                return null;
            }

            return (PassKind) results.get(0); //zakładam, że istnieje w bazie tylko jeden rodzaj karnetu o danej nazwie
        }
        catch (RuntimeException e)
        {
            e.printStackTrace();
        }
        finally
        {
            session.close();
        }

        return null;
    }
}
