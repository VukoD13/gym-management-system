package repositories;

import domain.Identifier;

/**
 *
 */
public interface IIdentifierRepository extends IRepository<Identifier>
{
    public Identifier getIdentifierByNumber(int number);
}
