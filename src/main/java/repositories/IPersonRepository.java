package repositories;

import domain.Person;

/**
 *
 */
public interface IPersonRepository extends IRepository<Person>
{
    public Person getPersonBySurnameAndName(String surname, String name);
}