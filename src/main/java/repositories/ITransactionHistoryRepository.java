package repositories;

import domain.TransactionHistory;

/**
 *
 */
public interface ITransactionHistoryRepository extends IRepository<TransactionHistory>
{}
