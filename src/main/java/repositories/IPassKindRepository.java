package repositories;

import domain.PassKind;

/**
 *
 */
public interface IPassKindRepository extends IRepository<PassKind>
{
    public PassKind getPassKindByName(String name);
}
