package beans;

import domain.Admin;
import domain.Identifier;
import domain.PassKind;
import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * Klasa aktualnei nieużywana!
 * Dodaje trochę rekordów do bazy danych, głównie w celach łatwego testowania
 */
//@Singleton
//@Startup
public class InitializeBean
{
    private Admin admin;
    private Person person;
    private PassKind passKind;

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public Admin getAdmin()
    {
        return admin;
    }

    public void setAdmin(Admin admin)
    {
        this.admin = admin;
    }

    public PassKind getPassKind()
    {
        return passKind;
    }

    public void setPassKind(PassKind passKind)
    {
        this.passKind = passKind;
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        addDefaultAdmin();
        addDefaultPerson();
        addDefaultPassKinds();
        addIdentifiers();
    }

    private void addDefaultAdmin()
    {
        this.admin = new Admin();
        admin.setDefault();

        catalog.getAdmins().add(admin);

        admin = null;
    }

    private void addDefaultPerson()
    {
        this.person = new Person();

        person.setName("t");
        person.setSurname("r");
        catalog.getPersons().add(person);

        person.setName("r");
        person.setSurname("e");
        catalog.getPersons().add(person);
    }

    private void addDefaultPassKinds()
    {
        this.passKind = new PassKind();

        passKind.setName("Klasyczny miesieczny");
        passKind.setPrice(159.99f);
        passKind.setValidityDays(30);
        catalog.getPassKinds().add(passKind);

        passKind.setName("Klasyczny studencki");
        passKind.setPrice(130.00f);
        passKind.setValidityDays(30);
        catalog.getPassKinds().add(passKind);

        passKind.setName("8 Wejsc");    //na miesiac
        passKind.setPrice(120.00f);
        passKind.setNumberOfEntrances(8);
        passKind.setValidityDays(30);
        catalog.getPassKinds().add(passKind);

        passKind.setName("Jednorazowy");
        passKind.setPrice(25.00f);
        passKind.setNumberOfEntrances(1);
        catalog.getPassKinds().add(passKind);
    }

    private void addIdentifiers()
    {
        final int numberOfIdentifiers = 10;
        Identifier identifier = new Identifier();
        for(int i = 1; i <= numberOfIdentifiers; i++)
        {
            identifier.setNumber(i);
            catalog.getIdentifiers().add(identifier);
        }
    }
}
