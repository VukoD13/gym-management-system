package beans;

import domain.Identifier;
import domain.Pass;
import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
/**
 * Klasa obsługująca system odbierania identyfikatorów
 */
@ManagedBean
@SessionScoped
public class TakeIdentifierBean
{
    private Identifier identifier;

    //dla strony jsf - pomocnicza zmienna przechowująca numer identyfikatora
    private int identifierNumber;

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    public void init()
    {
        this.identifier = new Identifier();
    }

    /**
     * funkcja wywołująca się przy oddawaniu przez klienta identyfikatora - klucz znowu jest dostępny dla kolejnej osoby
     */
    public void collectIdentifier()
    {
        identifier = catalog.getIdentifiers().getIdentifierByNumber(identifierNumber);

        if(identifier != null)
        {
            Person person = identifier.getPerson();
            if(person != null)
            {
                identifier.setPerson(null);
                catalog.getIdentifiers().update(identifier);

                subNumberOfEntrances(person);

                person.setIsIn(false);
                catalog.getPersons().update(person);

                FacesContext.getCurrentInstance().addMessage("takeIdentifierForm:takeIdentifierButton", new FacesMessage(" Sukces"));
            }
            else
            {
                FacesContext.getCurrentInstance().addMessage("takeIdentifierForm:takeIdentifierButton", new FacesMessage(" Błąd! Spróbuj ponownie (zły numer?)"));
            }
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage("takeIdentifierForm:takeIdentifierButton", new FacesMessage(" Błąd! Klucz o takim numerze nie istnieje"));
        }
    }

    /**
     * zmniejsza o 1 liczbę pozostałych wejść na karnecie
     */
    private void subNumberOfEntrances(Person person)
    {
        if(person.getPass().getNumberOfEntrances() == null)
        {
            return;
        }
        person.getPass().setNumberOfEntrances(person.getPass().getNumberOfEntrances() - 1);

        catalog.getPasses().update(person.getPass());

        //jeśli liczba pozostałych wejść <= 0 to usuń bezużtyczeny karnet
        if(person.getPass().getNumberOfEntrances() <= 0)
        {
            Pass pass = person.getPass();
            person.setPass(null);
            catalog.getPersons().update(person);
            catalog.getPasses().delete(pass);
        }
    }

    /////////////////////////////////////getters / setters/////////////////////////////////////

    public Identifier getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(Identifier identifier)
    {
        this.identifier = identifier;
    }

    public int getIdentifierNumber()
    {
        return identifierNumber;
    }

    public void setIdentifierNumber(int identifierNumber)
    {
        this.identifierNumber = identifierNumber;
    }
}
