package beans;

import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 * Sesyjny bean, odpowiedzialny za wyszukiwanie klienta. Sesyjny, ponieważ jego atrybuty musimy wyświetlić na kolejnej stronie jsf po wywołaniu funkcji, która je zmienia
 */
@ManagedBean
@SessionScoped
public class PersonBean
{
    private Person person;

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        this.person = new Person();
    }

    /**
     * Wyszukiwanie osoby
     * @return przekierowanie do strony zarządzania daną osobą przy powodzeniu
     */
    public String findPerson()
    {
        person = catalog.getPersons().getPersonBySurnameAndName(person.getSurname(), person.getName());

        if(person == null)
        {
            person = new Person();  //person nie może być null, jest w sesji ciągle używany
            FacesContext.getCurrentInstance().addMessage("findPersonForm:findPersonButton", new FacesMessage(" Błąd! Osoba nie istnieje w bazie"));
            return null;
        }

        return "personManagement.xhtml";
    }

    /**
     * Dodanie nowej osoby
     * @return przekierowanie do strony zarządzania daną osobą
     */
    public String addNewPerson()
    {
        if(catalog.getPersons().getPersonBySurnameAndName(person.getSurname(), person.getName()) == null)
        {
            catalog.getPersons().add(person);
            return "personManagement.xhtml";
        }
        else
            FacesContext.getCurrentInstance().addMessage("addPersonForm:addPersonButton", new FacesMessage(" Błąd! Osoba już istnieje w bazie"));

        return null;
    }
}
