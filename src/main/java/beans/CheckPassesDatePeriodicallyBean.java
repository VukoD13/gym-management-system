package beans;

import domain.Pass;
import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * klasa odpowiedzialna za sprawdzanie co jakiś czas dat karnetów i ich ewentualne usuwanie, jęsli są nieaktualne
 */
@Singleton
@Startup
public class CheckPassesDatePeriodicallyBean
{
    private final int checkingPassesHour = 23;

    private class Task extends TimerTask
    {
        /**
         * sprawdzam wszystkie karnety czy mają wciąż aktualną datę ważności
         * UWAGA - robię to w bardzo nieoptymalny sposób, żeby usprawnić to musiałbym zmienić lekko klasę Pass i prawdopodobnie Person. Trochę źle to po prostu przemyślałem
         * @param date aktualna data
         */
        private void checkPasses(Date date)
        {
            List<Pass> passes = catalog.getPasses().getAll(Pass.class);
            List<Person> persons = catalog.getPersons().getAll(Person.class);
            for(Pass pass : passes)
            {
                if( (pass.getExpireDate() != null) && (pass.getExpireDate().before(date)) )
                {
                    for(Person person : persons)
                    {
                        if(person.getPass().getId() == pass.getId())
                        {
                            person.setPass(null);
                            catalog.getPersons().update(person);
                            break;
                        }
                    }

                    catalog.getPasses().delete(pass);
                }
            }
        }

        @Override
        public void run()
        {
            Date date = new Date();
            date.setTime(date.getTime());
            System.out.println(date.getHours() + " " + date.getMinutes() + " " + date.getSeconds());

            if(date.getHours() == checkingPassesHour)
                checkPasses(date);
        }
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        Timer timer = new Timer();
        Task timer_task = new Task();
        timer.schedule (timer_task, 0, TimeUnit.MINUTES.toMillis(60));  //funkcja odpala się o okreslonej godzinie, więc przynajmniej raz na godzinę należy ją uruchomić
    }
}
