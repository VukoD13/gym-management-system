package beans.config;

import domain.Admin;
import domain.PassKind;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

/**
 * gdy pierwszy raz uruchomimy system (nie ma żadnych kont admina) pozwoli stworzyć główne konto admina
 */
@ManagedBean
@RequestScoped
public class OnStartupBean
{
    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        List<PassKind> passKinds = catalog.getPassKinds().getAll(PassKind.class);
        if(passKinds.size() == 0)
            addDefaultPassKinds();
    }

    /**
     * jeśli istnieje jakiś admin przenosi do strony logowania
     */
    public void redirect()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        if (isAnyAdminExisting())
        {
            ConfigurableNavigationHandler configurableNavigationHandler
                    = (ConfigurableNavigationHandler)
                    facesContext.getApplication().getNavigationHandler();

            configurableNavigationHandler.performNavigation("login.xhtml");
        }
    }

    /**
     * Sprawdza czy istnieje jakikolwiek admin w bazie
     * @return true jeśli istnieje
     */
    private Boolean isAnyAdminExisting()
    {
        List<Admin> admins = catalog.getAdmins().getAll(Admin.class);

        if(admins.size() == 0)
            return false;
        else
            return true;
    }

    /**
     * Dodaje domyślne rodzaje karnetów
     */
    private void addDefaultPassKinds()
    {
        PassKind passKind = new PassKind();

        passKind.setName("Klasyczny miesieczny");
        passKind.setPrice(159.99f);
        passKind.setValidityDays(30);
        catalog.getPassKinds().add(passKind);

        passKind.setName("Klasyczny studencki");
        passKind.setPrice(130.00f);
        passKind.setValidityDays(30);
        catalog.getPassKinds().add(passKind);

        passKind.setName("8 Wejsc");    //ważny jeden dzień - dla testów
        passKind.setPrice(120.00f);
        passKind.setNumberOfEntrances(8);
        passKind.setValidityDays(1);
        catalog.getPassKinds().add(passKind);

        passKind.setName("Jednorazowy");
        passKind.setPrice(25.00f);
        passKind.setNumberOfEntrances(1);
        catalog.getPassKinds().add(passKind);
    }
}
