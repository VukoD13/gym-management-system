package beans.config;

import domain.PassKind;
import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

/**
 * Pozwala na dodawanie oraz usuwanie rodzajów karnetów dostępnych na siłowni
 */
@ManagedBean
@RequestScoped
public class PassKindManagementBean
{
    PassKind passKind;

    /**
     * potrzebne do wyświetlenia listy rodzajów karnetów
     */
    public List<PassKind> getPassKinds()
    {
        return catalog.getPassKinds().getAll(PassKind.class);
    }

    public void deletePassKind(String name)
    {
        List<Person> persons = catalog.getPersons().getAll(Person.class);

        Boolean isDeletingAllowed = true;
        for(Person person : persons)
        {
            if(person.getPass() != null && person.getPass().getPassKind().getName().equals(name))
            {
                isDeletingAllowed = false;
                break;
            }
        }

        if(isDeletingAllowed)
        {
            PassKind passKind = catalog.getPassKinds().getPassKindByName(name);
            catalog.getPassKinds().delete(passKind);
            FacesContext.getCurrentInstance().addMessage("deletePassKindForm:deletePassKindButton", new FacesMessage(" Sukces"));
        }
        else
            FacesContext.getCurrentInstance().addMessage("deletePassKindForm:deletePassKindButton", new FacesMessage(" Błąd! Ktoś używa karnetu z tym rodzajem!"));
    }

    public void addPassKind()
    {
        if(catalog.getPassKinds().getPassKindByName(passKind.getName()) == null)
        {
            if(passKind.getNumberOfEntrances() < 0)
                passKind.setNumberOfEntrances(0);

            catalog.getPassKinds().add(passKind);
            FacesContext.getCurrentInstance().addMessage("addPassKindForm:addPassKindButton", new FacesMessage(" Sukces"));
        }
        else
            FacesContext.getCurrentInstance().addMessage("addPassKindForm:addPassKindButton", new FacesMessage(" Błąd! Rodzaj karnetu o takiej nazwie już istnieje w bazie"));
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        passKind = new PassKind();
    }

    public PassKind getPassKind()
    {
        return passKind;
    }

    public void setPassKind(PassKind passKind)
    {
        this.passKind = passKind;
    }
}
