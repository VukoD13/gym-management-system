package beans.config;

import domain.Admin;
import domain.Identifier;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 * Bean obsługujący stronę konfiguracyjną
 */
@ManagedBean
@RequestScoped
public class ConfigBean
{
    private Admin admin;
    private int numberOfIdentifiers;

    private void addAdmin()
    {
        catalog.getAdmins().add(admin);
    }

    private void addIdentifiers()
    {
        Identifier identifier = new Identifier();
        for(int i = 1; i <= numberOfIdentifiers; i++)
        {
            identifier.setNumber(i);
            catalog.getIdentifiers().add(identifier);
        }
    }

    public String moveOn()
    {
        addAdmin();
        addIdentifiers();
        return "login.xhtml";
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        admin = new Admin();
    }

    public Admin getAdmin()
    {
        return admin;
    }

    public void setAdmin(Admin admin)
    {
        this.admin = admin;
    }

    public int getNumberOfIdentifiers()
    {
        return numberOfIdentifiers;
    }

    public void setNumberOfIdentifiers(int numberOfIdentifiers)
    {
        this.numberOfIdentifiers = numberOfIdentifiers;
    }
}
