package beans.config;

import domain.Admin;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 */
@ManagedBean
@RequestScoped
public class AddAdminBean
{
    Admin admin;

    public void addAdmin()
    {
        if(catalog.getAdmins().getAdminByLogin(admin.getLogin()) == null)
        {
            catalog.getAdmins().add(admin);
            FacesContext.getCurrentInstance().addMessage("addAdminForm:addAdminButton", new FacesMessage(" Sukces"));
        }
        else
            FacesContext.getCurrentInstance().addMessage("addAdminForm:addAdminButton", new FacesMessage(" Błąd! Administrator o takim loginie już istnieje w bazie"));
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        admin = new Admin();
    }

    public Admin getAdmin()
    {
        return admin;
    }

    public void setAdmin(Admin admin)
    {
        this.admin = admin;
    }
}
