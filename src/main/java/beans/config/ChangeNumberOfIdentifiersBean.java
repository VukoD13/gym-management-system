package beans.config;

import domain.Identifier;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

/**
 * Pozwala na zmianę liczby identyfikatorów posiadanych przez siłownię
 */
@ManagedBean
@RequestScoped
public class ChangeNumberOfIdentifiersBean
{
    private int newNumberOfIdentifiers;

    private void addIdentifiers(int numberOfIdentifiers)
    {
        Identifier identifier = new Identifier();
        for(int i = numberOfIdentifiers + 1; i <= newNumberOfIdentifiers; i++)
        {
            identifier.setNumber(i);
            catalog.getIdentifiers().add(identifier);
        }
        FacesContext.getCurrentInstance().addMessage("changeNumberOfIdentifiersForm:changeNumberOfIdentifiersButton", new FacesMessage(" Sukces"));
    }

    private void deleteIdentifiers(int numberOfIdentifiers, List<Identifier> identifiers)
    {
        Identifier identifier;
        List<Identifier> identifierList = new LinkedList<Identifier>();
        for(int i = numberOfIdentifiers; i > newNumberOfIdentifiers; i--)
        {
            if(identifiers.get(i-1).getPerson() == null)        //i - 1 ponieważ lista liczona od 0, a numery od 1
            {
                identifierList.add(identifiers.get(i-1));
            }
            else break;
        }

        if(identifierList.size() == numberOfIdentifiers - newNumberOfIdentifiers)
        {
            for(Identifier id : identifierList)
            {
                catalog.getIdentifiers().delete(id);
            }
            FacesContext.getCurrentInstance().addMessage("changeNumberOfIdentifiersForm:changeNumberOfIdentifiersButton", new FacesMessage(" Sukces"));
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage("changeNumberOfIdentifiersForm:changeNumberOfIdentifiersButton", new FacesMessage(" Błąd! Któryś z identyfikatorów, który powinien zostać usunięty jest zajęty"));
        }

    }

    public void changeNumberOfIdentifiers()
    {
        List<Identifier> identifiers = catalog.getIdentifiers().getAll(Identifier.class);
        int numberOfIdentifiers = identifiers.size();

        if(numberOfIdentifiers < newNumberOfIdentifiers)
        {
            addIdentifiers(numberOfIdentifiers);
        }
        else if(numberOfIdentifiers > newNumberOfIdentifiers)
        {
            deleteIdentifiers(numberOfIdentifiers, identifiers);
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage("changeNumberOfIdentifiersForm:changeNumberOfIdentifiersButton", new FacesMessage(" Błąd. Aktualnie siłownia wykorzystuje już podaną liczbę identyfikatorów"));
        }
    }

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
    }

    public int getNewNumberOfIdentifiers()
    {
        return newNumberOfIdentifiers;
    }

    public void setNewNumberOfIdentifiers(int newNumberOfIdentifiers)
    {
        this.newNumberOfIdentifiers = newNumberOfIdentifiers;
    }
}
