package beans;

import domain.Pass;
import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 * Klasa odpowiedzialna za pobieranie karnetu danej osoby, pomocna w wyświetlaniu na stronie jsf
 */
@ManagedBean
@RequestScoped
public class ShowPassBean
{
    private Pass pass;

    //wyodrębnienie beanu, który zawiera informacje o osobie której karnet chcemy wyświetlić
    @ManagedProperty(value="#{personBean.person}")
    private Person person;

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        pass = person.getPass();
        if(pass == null)
        {
            pass = new Pass();
        }
        if((Integer)pass.getNumberOfEntrances() == null)
        {
            pass.setNumberOfEntrances(new Integer(0));
        }
    }

    /////////////////////////////////////getters / setters/////////////////////////////////////

    public Pass getPass()
    {
        return pass;
    }

    public void setPass(Pass pass)
    {
        this.pass = pass;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }
}
