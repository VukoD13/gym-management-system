package beans;

import domain.Pass;
import domain.PassKind;
import domain.Person;
import domain.TransactionHistory;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Odpowiedzialny za dodawanie karnetów dla klientów
 */
@ManagedBean
@SessionScoped
public class AddPassBean
{
    private Pass pass;

    //wyodrębnienie beanu, który zawiera informacje o osobie dla której chcemy dodać nowy karnet
    @ManagedProperty(value="#{personBean}")
    private PersonBean personBean;
    private Person person;

    //zmienne String obsługują selectonemenu (dropdownmenu), po wyborze ręcznie konwertuję je do obiektów
    private String passKindName;
    private List<String> passKindNameList;
    private PassKind passKind;
    private List<PassKind> passKindList;

    //zmienna pomocnicza do wyświetlenia dynamicznego formularza jsf
    private Boolean isDropdownMenuVisible;

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    private void init()
    {
        person = personBean.getPerson();
        pass = new Pass();
        passKind = new PassKind();
        isDropdownMenuVisible = false;

        getAllPassKinds();
    }

    /**
     * pobiera z bazy danych rodzaje karnetów i zapisuje je do listy w formie "nazwa cena"
     */
    private void getAllPassKinds()
    {
        passKindList = catalog.getPassKinds().getAll(PassKind.class);

        passKindNameList = new LinkedList<String>();
        for(PassKind pk : passKindList)
        {
            //wypełnianie listy nazwami karnetów, uwcześnie pobranych z bazy danych
            passKindNameList.add( pk.getName() + " " + Float.toString(pk.getPrice()) );
        }
    }

    /**
     * zabezpieczenie, gdyby ktoś jakimś cudem chciał kupić nowy karnet wciąż mając stary, nie potrzebujemy śmieci w bazie danych, więc trzeba go usunąć i zaktualizować daną osobę
     */
    private void deletePassIfAlreadyExist()
    {
        Pass p = person.getPass();
        if(p != null)
        {
            person.setPass(null);
            catalog.getPersons().update(person);
            catalog.getPasses().delete(p);
        }
    }

    /**
     * Tworzy karnet, wrzuca do bazy danych oraz wywołuje funkcję przyposującą karnet do osoby
     * Złamałem pojedyczną odpowiedzialność, jednak nie widzę chwilowo poprawiać tej funkcji (zmienię, gdyby okazało się, że trzeba tu jeszcze modyfikować coś większego lub dodawać)
     */
    public void addNewPass()
    {
        person = personBean.getPerson();

        deletePassIfAlreadyExist();
        //uzyskanie obiektu PassKind posiadając informację o jego nazwie
        for(PassKind pk : passKindList)
        {
            //Zamiana Stringa z nazwą i ceną karnetu na samą nazwę, by móc potem zamienić to na obiekt
            String string[] = passKindName.split(" ");
            String str = "";
            for(int i = 0; i < string.length-1; i++)
            {
                if(i > 0)
                    str+=" ";
                str+= string[i];
            }

            if(pk.getName().equals(str))
            {
                //ostateczne ustawienie rodzaju karnetu
                passKind = pk;
                break;
            }
        }

        pass.setPassKind(passKind);
        pass.setNumberOfEntrances( (passKind.getNumberOfEntrances() > 0) ? passKind.getNumberOfEntrances() : null );
        Date date = new Date();
        date.setTime(date.getTime() + TimeUnit.DAYS.toMillis(passKind.getValidityDays()));
        pass.setExpireDate(date);
        catalog.getPasses().add(pass);

        addPassToPerson();

        addRecordToTransactionHistory(date);

        isDropdownMenuVisible = false;
    }

    /**
     * przypisuje karnet do klienta
     */
    private void addPassToPerson()
    {
        person.setPass(pass);
        catalog.getPersons().update(person);
    }

    /**
     * dodaje nowy rekord do historii transakcji
     * @param date data dokonania transakcji
     */
    private void addRecordToTransactionHistory(Date date)
    {
        TransactionHistory transactionHistoryRecord = new TransactionHistory();
        transactionHistoryRecord.setPerson(person);
        transactionHistoryRecord.setPassKind(passKind);
        transactionHistoryRecord.setPrice(passKind.getPrice());
        transactionHistoryRecord.setDate(date);

        catalog.getTransactionHistories().add(transactionHistoryRecord);
    }

    /////////////////////////////////////getters / setters/////////////////////////////////////

    public Pass getPass()
    {
        return pass;
    }

    public void setPass(Pass pass)
    {
        this.pass = pass;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }

    public PassKind getPassKind()
    {
        return passKind;
    }

    public void setPassKind(PassKind passKind)
    {
        this.passKind = passKind;
    }

    public List<PassKind> getPassKindList()
    {
        return passKindList;
    }

    public void setPassKindList(List<PassKind> passKindList)
    {
        this.passKindList = passKindList;
    }

    public String getPassKindName()
    {
        return passKindName;
    }

    public void setPassKindName(String passKindName)
    {
        this.passKindName = passKindName;
    }

    public List<String> getPassKindNameList()
    {
        return passKindNameList;
    }

    public void setPassKindNameList(List<String> passKindNameList)
    {
        this.passKindNameList = passKindNameList;
    }

    public Boolean getIsDropdownMenuVisible()
    {
        return isDropdownMenuVisible;
    }

    public void setIsDropdownMenuVisible(Boolean isDropdownMenuVisible)
    {
        this.isDropdownMenuVisible = isDropdownMenuVisible;
    }

    public PersonBean getPersonBean()
    {
        return personBean;
    }

    public void setPersonBean(PersonBean personBean)
    {
        this.personBean = personBean;
    }
}
