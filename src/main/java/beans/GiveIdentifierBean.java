package beans;

import domain.Identifier;
import domain.Pass;
import domain.Person;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Klasa obsługująca system dawania identyfikatorów
 */
@ManagedBean
@RequestScoped
public class GiveIdentifierBean
{
    private Identifier identifier;
    private List<Identifier> identifiers;

    @ManagedProperty(value="#{personBean.person}")
    private Person person;

    @Inject
    IRepositoryCatalog catalog;

    @PostConstruct
    public void init()
    {
        this.identifier = new Identifier();
    }

    /**
     * sprawdza, czy dany karnet jest ważny - tylko datę, ponieważ przy wychodzeniu klienta zmniejszana jest zmienna numberOfEntrances i ewentualnie karnet usuwany gdy wartość zmiennej = 0
     */
    private Boolean isPassUpToDate()
    {
        Pass pass = person.getPass();
        Date date = new Date();
        date.setTime(date.getTime());

        //jeśli data ważności karnetu przeminęła, usuń karnet i zwróć false
        if( (pass.getExpireDate() != null) && (pass.getExpireDate().before(date)) )
        {
            person.setPass(null);
            catalog.getPersons().update(person);
            catalog.getPasses().delete(pass);
            return false;
        }
        return true;
    }

    public void giveIdentifier()
    {
        //czy klient aby na pewno nie ma juz przypisanego identyfikatora
        if( person.getIsIn() )
        {
            identifiers = catalog.getIdentifiers().getAll(Identifier.class);
            for(Identifier id : identifiers)
            {
                if( id.getPerson() != null )
                if( id.getPerson().getId() == person.getId())
                {
                    FacesContext.getCurrentInstance().addMessage("giveIdentifierForm:giveIdentifierButton", new FacesMessage(" Błąd! Klient ma już przypisany identyfikator i powinien być na terenie placówki. Numer idemtyfikatora: " + Integer.toString(id.getNumber())));
                    return;
                }
            }
        }

        //czy klient w ogole posiada karnet
        if(person.getPass() != null)
        {
            if(isPassUpToDate())
            {
                person.setIsIn(true);
                catalog.getPersons().update(person);

                randIdentifier();
            }
            else
            {
                catalog.getPasses().delete(person.getPass());
                FacesContext.getCurrentInstance().addMessage("giveIdentifierForm:giveIdentifierButton", new FacesMessage(" Błąd! Klient nie ma ważnego karnetu"));
            }
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage("giveIdentifierForm:giveIdentifierButton", new FacesMessage(" Błąd! Klient nie ma karnetu"));
        }
    }

    /**
     * losuje numer identyfikatora dla wchodzącego klienta
     */
    private void randIdentifier()
    {
        //dodaj do listy wolne klucze
        identifiers = new LinkedList<Identifier>();
        identifiers = catalog.getIdentifiers().getAll(Identifier.class);
        //mogłem użyć CopyOnWriteArrayList, jednak w celach wydajnościowych wykorzystałem prościutkie rozwiązanie i--
        //for(Identifier id : identifiers)
        for(int i = 0; i < identifiers.size(); i++)
        {
            Identifier id = identifiers.get(i);
            if(id.getPerson() != null)
            {
                identifiers.remove(id);
                i--;
            }
        }

        //jesli nie ma kluczy na liscie = brak wolnych kluczy
        if(identifiers.size() == 0)
        {
            FacesContext.getCurrentInstance().addMessage("giveIdentifierForm:giveIdentifierButton", new FacesMessage(" Brak wolnych identyfikatorów!"));
            //return;
        }

        else
        {
            //losuj numer
            Random rand = new Random();
            int randomNumber = rand.nextInt(identifiers.size());
            identifier = identifiers.get(randomNumber);

            assignIdentifier();

            FacesContext.getCurrentInstance().addMessage("giveIdentifierForm:giveIdentifierButton", new FacesMessage(" Numer identyfikatora: " + Integer.toString(identifier.getNumber())));
        }
    }

    /**
     * przypisanie identyfikatora do klienta
     */
    private void assignIdentifier()
    {
        identifier.setPerson(person);
        catalog.getIdentifiers().update(identifier);
    }

    /////////////////////////////////////getters / setters/////////////////////////////////////

    public Identifier getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(Identifier identifier)
    {
        this.identifier = identifier;
    }

    public List<Identifier> getIdentifiers()
    {
        return identifiers;
    }

    public void setIdentifiers(List<Identifier> identifiers)
    {
        this.identifiers = identifiers;
    }

    public Person getPerson()
    {
        return person;
    }

    public void setPerson(Person person)
    {
        this.person = person;
    }
}
