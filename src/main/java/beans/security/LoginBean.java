package beans.security;

import domain.Admin;
import repositories.IRepositoryCatalog;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Bean obsługujący system logowania
 */
@ManagedBean
@SessionScoped
public class LoginBean implements Serializable
{
    private static final long serialVersionUID = 7765876811740798583L;

    private String login;
    private String password;

    private Boolean isLogged;

    @Inject
    IRepositoryCatalog catalog;

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public Boolean getIsLogged()
    {
        return isLogged;
    }

    public void setIsLogged(Boolean isLogged)
    {
        this.isLogged = isLogged;
    }

    @PostConstruct
    private void init()
    {
        isLogged = false;
    }

    public String doLogin()
    {
        List<Admin> admins = catalog.getAdmins().getAll(Admin.class);
        for(Admin admin : admins)
        {
            if (admin.getLogin().equals(login) && admin.getPassword().equals(password))
            {
                isLogged = true;

                return "/secured/home.xhtml";
            }
        }

        FacesContext.getCurrentInstance().addMessage("loginForm:loginButton", new FacesMessage(" Błąd logowania!"));

        return null;
    }

    public String doLogout()
    {
        isLogged = false;

        return "../login.xhtml";
    }
}
